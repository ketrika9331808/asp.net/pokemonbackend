﻿using System;
using Microsoft.EntityFrameworkCore;
using UserCRUD.Data;
using UserCRUD.Interfaces;
using UserCRUD.Models;

namespace UserCRUD.Repositories
{
	public class UserRepository: IUserRepository
	{
        private readonly DataContext _context;

        public UserRepository(DataContext context)
		{
            _context = context;

        }

        public Task<bool> CreateUser(User user)
        {
            _context.AddAsync(user);

            return Save();
        }

        public Task<bool> DeleteUser(User user)
        {
            _context.Remove(user);

            return Save();
        }

        public async Task<User> GetUser(int userId)
        {
            return await _context.Users.FirstOrDefaultAsync(u => u.Id == userId);
        }

        public async Task<User> GetUser(string username)
        {
            return await _context.Users.FirstOrDefaultAsync(u => u.Username.Trim().ToLower() == username.Trim().ToLower());
        }

        public async Task<ICollection<User>> GetUsers()
        {
            return await _context.Users.ToListAsync();

        }

        public async Task<bool> Save()
        {
            var saved = await _context.SaveChangesAsync();

            return saved > 0 ? true : false;

        }

        public async Task<bool> UserExists(string username)
        {
            return await _context.Users.AnyAsync(u => u.Username == username);
        }

        public async Task<bool> UserExists(int userId)
        {
            return await _context.Users.AnyAsync(u => u.Id == userId);
        }
    }
}

