﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace UserCRUD.Migrations
{
    /// <inheritdoc />
    public partial class UpdateForeignKeyPokemonCategory : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PokemonCategories_Categories_PokemonId",
                table: "PokemonCategories");

            migrationBuilder.CreateIndex(
                name: "IX_PokemonCategories_CategoryId",
                table: "PokemonCategories",
                column: "CategoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_PokemonCategories_Categories_CategoryId",
                table: "PokemonCategories",
                column: "CategoryId",
                principalTable: "Categories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PokemonCategories_Categories_CategoryId",
                table: "PokemonCategories");

            migrationBuilder.DropIndex(
                name: "IX_PokemonCategories_CategoryId",
                table: "PokemonCategories");

            migrationBuilder.AddForeignKey(
                name: "FK_PokemonCategories_Categories_PokemonId",
                table: "PokemonCategories",
                column: "PokemonId",
                principalTable: "Categories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
