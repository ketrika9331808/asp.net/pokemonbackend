﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UserCRUD.Models
{
	public class Country
	{
        public int Id { get; set; }
        public required string Name { get; set; }
        public ICollection<Owner> Owners { get; set; }
    }
}

