﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UserCRUD.Models
{
	public class Category
	{
        public int Id { get; set; }
        public required string Name { get; set; }
        public ICollection<PokemonCategory> PokemonCategories { get; set; }
    }
}

