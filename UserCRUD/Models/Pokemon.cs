﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UserCRUD.Models
{
	public class Pokemon
	{
        public int Id { get; set; }
		public required string Name { get; set; }
		public DateTime BirthDate { get; set; }
		public ICollection<Review> Reviews { get; set; }
		public ICollection<PokemonCategory> PokemonCategories { get; set; }
        public ICollection<PokemonOwner> PokemonOwners { get; set; }
    }
}

