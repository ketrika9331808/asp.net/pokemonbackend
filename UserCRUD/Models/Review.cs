﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UserCRUD.Models
{
	public class Review
	{
        public int Id { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public int Rating { get; set; }
        public Pokemon Pokemon { get; set; }
        public Reviewer Reviewer { get; set; }
    }
}

