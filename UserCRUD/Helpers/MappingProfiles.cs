﻿using System;
using AutoMapper;
using UserCRUD.Dto;
using UserCRUD.Models;

namespace UserCRUD.Helpers
{
	public class MappingProfiles: Profile
	{
		public MappingProfiles()
		{
			CreateMap<Pokemon, PokemonDto>().ReverseMap();
			CreateMap<Category, CategoryDto>().ReverseMap();
            CreateMap<Country, CountryDto>().ReverseMap();
			CreateMap<Owner, OwnerDto>().ReverseMap();
            CreateMap<Review, ReviewDto>().ReverseMap();
			CreateMap<Reviewer, ReviewerDto>().ReverseMap();
			CreateMap<Reviewer, GetReviewerDto>();
            CreateMap<User, UserDto>();
        }
	}
}

