﻿using System;
namespace UserCRUD.Dto
{
	public class UserLoginResponse
	{
        public int Id { get; set; }
        public string Username { get; set; }
        public string Role { get; set; }
        public string AuthToken { get; set; }
    }
}

