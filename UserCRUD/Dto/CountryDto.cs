﻿using System;
namespace UserCRUD.Dto
{
	public class CountryDto
	{
        public int Id { get; set; }
        public required string Name { get; set; }
    }
}

