﻿using System;

namespace UserCRUD.Dto
{
	public class ReviewerDto
	{
        public int Id { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
    }
}

