﻿using System;
namespace UserCRUD.Dto
{
	public class GetReviewerDto
	{
        public int Id { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public ICollection<ReviewDto> Reviews { get; set; }
    }
}

