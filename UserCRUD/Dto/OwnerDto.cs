﻿using System;
namespace UserCRUD.Dto
{
	public class OwnerDto
	{
        public int Id { get; set; }
        public required string Name { get; set; }
        public string Gym { get; set; }
    }
}

