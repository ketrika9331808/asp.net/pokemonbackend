﻿using System;
namespace UserCRUD.Dto
{
	public class UserDto
	{
        public int Id { get; set; }
        public required string Username { get; set; }
        public string Role { get; set; }
    }
}

