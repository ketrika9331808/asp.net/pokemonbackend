﻿using System;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using UserCRUD.Dto;
using UserCRUD.Interfaces;
using UserCRUD.Models;

namespace UserCRUD.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UserController: Controller
	{
        private readonly IUserRepository _userRepository;
        private readonly IPasswordService _passwordService;
        private readonly IMapper _mapper;

        public UserController(IUserRepository userRepository, ITokenService tokenService, IPasswordService passwordService, IMapper mapper)
		{
            _userRepository = userRepository;
            _passwordService = passwordService;
            _mapper = mapper;
		}

        [HttpGet]
        [ProducesResponseType(200, Type = typeof(IEnumerable<User>))]
        public async Task<IActionResult> GetUsers()
        {
            var users = _mapper.Map<List<UserDto>>(await _userRepository.GetUsers());

            if (!ModelState.IsValid) return BadRequest(ModelState);

            return Ok(users);
        }

        [HttpGet("{userId}")]
        [ProducesResponseType(200, Type = typeof(User))]
        public async Task<IActionResult> GetUser(int userId)
        {
            if (!await _userRepository.UserExists(userId)) return NotFound();

            var user = _mapper.Map<UserDto>(await _userRepository.GetUser(userId));

            if (!ModelState.IsValid) return BadRequest(ModelState);

            return Ok(user);
        }
    }
}

