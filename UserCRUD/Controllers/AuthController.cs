﻿using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using UserCRUD.Dto;
using UserCRUD.Interfaces;
using UserCRUD.Models;

namespace UserCRUD.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController: Controller
	{
        private readonly IUserRepository _userRepository;
        private readonly ITokenService _tokenService;
        private readonly IPasswordService _passwordService;
 
        public AuthController(IUserRepository userRepository, ITokenService tokenService, IPasswordService passwordService)
        {
            _userRepository = userRepository;
            _tokenService = tokenService;
            _passwordService = passwordService;
        }
      

        [AllowAnonymous]
        [HttpPost("login")]
        [ProducesResponseType(200, Type = typeof(UserLoginResponse))]
        [ProducesResponseType(404)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Login([FromBody] UserLoginRequest userLoginRequest)
        {
            var user = await _userRepository.GetUser(userLoginRequest.Username);

            if (user == null)
            {
                ModelState.AddModelError("Username", "Username not found");
                return NotFound(ModelState);
            }

            if (!_passwordService.VerifyPassword(userLoginRequest.Password, user.Password))
            {
                ModelState.AddModelError("Password", "Incorrect password");
                return BadRequest(ModelState);
            }

            if (!ModelState.IsValid) return BadRequest(ModelState);

            var token = _tokenService.GenerateToken(user);

            var loginResponse = new UserLoginResponse
            {
                Id = user.Id,
                AuthToken = token,
                Role = user.Role,
                Username = user.Username
            };


            return Ok(loginResponse);
        }

        [HttpPost("signup")]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> CreateUser([FromBody] User userCreate)
        {
            if (userCreate == null) return BadRequest(ModelState);

            if (await _userRepository.UserExists(userCreate.Username))
            {
                ModelState.AddModelError("", "Reviewer with the same full name already exists");
                return StatusCode(422, ModelState);
            }

            if (!ModelState.IsValid) return BadRequest(ModelState);

            userCreate.Password = _passwordService.HashPassword(userCreate.Password);

            if (!await _userRepository.CreateUser(userCreate))
            {
                ModelState.AddModelError("", "Something went wrong while saving");
                return StatusCode(500, ModelState);
            }

            var token = _tokenService.GenerateToken(userCreate);

            var loginResponse = new UserLoginResponse
            {
                Id = userCreate.Id,
                AuthToken = token,
                Role = userCreate.Role,
                Username = userCreate.Username
            };

            return Ok(loginResponse);
        }
    }
}

