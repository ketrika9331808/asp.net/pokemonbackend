﻿using System;
using UserCRUD.Models;

namespace UserCRUD.Interfaces
{
	public interface IOwnerRepository
	{
		ICollection<Owner> GetOwners();
		Owner GetOwner(int id);
		ICollection<Owner> GetOwnersOfAPokemon(int pokeId);
		ICollection<Pokemon> GetPokemonsByOwner(int ownerId);
		bool OwnerExists(int ownerId);
        bool CreateOwner(Owner owner);
        bool UpdateOwner(Owner owner);
        bool DeleteOwner(Owner owner);
        bool Save();
    }
}

