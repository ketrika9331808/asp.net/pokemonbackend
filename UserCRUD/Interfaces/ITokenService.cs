﻿using System;
using UserCRUD.Models;

namespace UserCRUD.Interfaces
{
	public interface ITokenService
	{
		string GenerateToken(User user);
	}
}

