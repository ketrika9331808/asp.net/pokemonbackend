﻿using System;
using UserCRUD.Models;

namespace UserCRUD.Interfaces
{
	public interface IUserRepository
	{
        Task<ICollection<User>> GetUsers();
        Task<User> GetUser(int userId);
        Task<User> GetUser(string username);
        Task<bool> CreateUser(User user);
        Task<bool> DeleteUser(User user);
        Task<bool> UserExists(string username);
        Task<bool> UserExists(int userId);
        Task<bool> Save();
    }
}

